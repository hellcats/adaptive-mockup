# Based on jupyter/scipy-notebook
# See here: https://hub.docker.com/r/jupyter/scipy-notebook/dockerfile


FROM jupyter/scipy-notebook@sha256:60b6dd2bf2347d260603d6609ddd97c3dd755f4c5e9fa8842a58855faf006328

# RUN conda install -qy \
# 	'ipycanvas' \
# 	'ipyevents' \
# 	'plotly' \
# 	'chart-studio' 

RUN conda install -qy \
	'k3d=2.7.2' 

WORKDIR /home/jovyan/work
COPY mockup.py Mockup.ipynb ./
COPY images/ ./images
