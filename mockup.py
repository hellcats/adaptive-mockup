from ipywidgets import Layout
import ipywidgets as widgets
import k3d

class MockUp:
	def __init__(self, log=None):
		self.log = log
		self.gray = Layout(width='120px', border='1px solid gray')
		self.textbox = Layout(width='200px')
		self.textstyle = {'description_width':'136px'}
		self.gap = widgets.Label(layout=Layout(width='6px', height='6px'))


	def show_sim(self):
		gray, textbox, textstyle, gap = self.gray, self.textbox, self.textstyle, self.gap
		green = Layout(border='1px solid green')

		title = widgets.HTML(value='<b>Adaptive Sim3D</b>')
		title_box = widgets.HBox([title], layout=Layout(justify_content='center'))

		import_but = widgets.FileUpload(description='Import STL', multiple=True, layout=gray)
		def import_stl(change):
			owner = change.owner
			for k,v in owner.value.items():
				self.plot += k3d.stl(v['content'])

			if self.log:
				with self.log:
					print(f'type = {list(change.new.keys())}')
		import_but.observe(import_stl, names='value')

		# slice1 = widgets.Label(value=)
		slice_title = widgets.Label(value='Slicing')
		slice1 = widgets.Text(description='Slice Thickness (um)', placeholder='100', style=textstyle, layout=textbox)
		slice2 = widgets.Text(description='Layers', placeholder='250', style=textstyle, layout=textbox)
		slice_but1 = widgets.Button(description='Compute Slices', layout=gray)
		slice_box = widgets.VBox([slice1, slice2, slice_but1], layout=Layout(width='208px', border='1px solid black'))

		sim_title = widgets.Label(value='Sim Settings')
		sim1 = widgets.Text(description='Resolution (px/um)', placeholder='4', style=textstyle, layout=textbox)
		sim2 = widgets.Text(description='Width (um)', placeholder='1000', style=textstyle, layout=textbox)
		sim3 = widgets.Text(description='Height (um)', placeholder='10000', style=textstyle, layout=textbox)
		sim4 = widgets.Text(description='Stage Speed (um/s)', placeholder='100', style=textstyle, layout=textbox)
		sim5 = widgets.Text(description='Passes', placeholder='1', style=textstyle, layout=textbox)
		sim6 = widgets.Text(description='Prefilter Radius (um)', placeholder='2', style=textstyle, layout=textbox)
		sim7 = widgets.Text(description='Gelation Flux', placeholder='1', style=textstyle, layout=textbox)
		sim_box = widgets.VBox([sim1, sim2, sim3, sim4, sim5, gap, sim6, sim7], layout=Layout(width='208px', border='1px solid black'))

		box1 = widgets.VBox([import_but, slice_title, slice_box, gap, sim_title, sim_box], width='208px')

		self.plot = k3d.plot(menu_visibility=False)
		self.plot.layout = Layout(width='512px', height='512px', border='1px solid black')
		plot_box = widgets.HBox([self.plot])

		box3 = widgets.HBox([box1, plot_box], layout=Layout(margin='0px 24px 0px 24px', justify_content='space-between'))

		run_but = widgets.Button(description='Run Sim', layout=gray)
		with open('images/playback_controls.png', 'rb') as inf:
			img = inf.read()
		run1 = widgets.Image(value=img, format='png', layout=Layout(height='18px'))
		run2 = widgets.IntSlider(value='20', layout=Layout(flex='1'))
		play_box = widgets.HBox([run1, run2], layout=Layout(width='512px', align_items='center'))
		box4 = widgets.HBox([run_but, play_box], layout=Layout(margin='0px 24px 0px 24px', justify_content='space-between'))

		panel = widgets.VBox([title_box, box3, box4], layout=Layout(width='800px', height='600px', border='1px solid black'))
		display(panel)

	def show_printer(self):
		gray, textbox, textstyle, gap = self.gray, self.textbox, self.textstyle, self.gap
		ipstyle = {'description_width':'112px'}

		title = widgets.HTML(value='<b>Adaptive3D Print Driver</b>')
		title_box = widgets.HBox([title], layout=Layout(justify_content='center'))

		# slice1 = widgets.Label(value=)
		calibrate_title = widgets.Label(value='Calibration')
		cal_but1 = widgets.Button(description='Print Test Part', layout=gray)
		cal1 = widgets.Text(description='Slice Thickness (um)', placeholder='100', style=textstyle, layout=textbox)
		cal2 = widgets.Text(description='Layers', placeholder='250', style=textstyle, layout=textbox)
		cal_box = widgets.VBox([cal_but1, cal1, cal2], layout=Layout(width='208px', border='1px solid black'))

		net_title = widgets.Label(value='Network')
		ips = [widgets.Text(description=f'IP {i}', placeholder=f'10.0.0.{i+10}', style=ipstyle, layout=textbox) for i in range(6)]
		if self.log: 
			with self.log:
				print(ips)
		net_box = widgets.VBox(ips, layout=Layout(width='208px', border='1px solid black'))

		man_title = widgets.Label(value='Manual Operation')
		with open('images/arrows.jpg', 'rb') as inf:
			img = inf.read()
		man1 = widgets.Image(value=img, format='jpg', layout=Layout(width='96px'))
		man_box = widgets.HBox([man1], layout=Layout(width='208px', border='1px solid black', justify_content='center'))

		box1 = widgets.VBox([calibrate_title, cal_box, gap, net_title, net_box, gap, man_title, man_box], width='208px')

		with open('images/sim_sample.png', 'rb') as inf:
			img = inf.read()
		plot = widgets.Image(value=img, format='png', layout=Layout(width='512px', height='512px', border='1px solid black'))
		plot_box = widgets.HBox([plot])

		box3 = widgets.HBox([box1, plot_box], layout=Layout(margin='0px 24px 0px 24px', justify_content='space-between'))

		print_but = widgets.Button(description='Print', layout=gray)

		stop_but = widgets.Button(description='Stop', button_style='danger', layout=gray)
		but_box = widgets.HBox([print_but, stop_but])
		box4 = widgets.HBox([gap, but_box], layout=Layout(margin='0px 24px 0px 24px', justify_content='space-between'))

		panel = widgets.VBox([title_box, box3, gap, box4], layout=Layout(width='800px', height='600px', border='1px solid black'))
		display(panel)

		pass